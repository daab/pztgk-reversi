﻿using UnityEngine;
using System.Collections;

public enum GameMode{

	PlayerVsPlayer,
	PlayerVsComputer,
	Multiplayer
}
