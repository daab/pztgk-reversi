﻿using UnityEngine;
using System.Collections;

public class HUD : MonoBehaviour {

	public GUIText player1Result;
	public GUIText player2Result;
	public GUIText timeResult;
	
	public float turnTime = 30;
	private float timer;
	private float elapsedTime = 0;

	// Use this for initialization
	void Start () {
		player1Result.fontStyle = FontStyle.Bold;
		this.timer = this.turnTime;
	}

	public void UpdateResults(Vector2 gameResults)
	{
		player1Result.text = string.Format ("{0}: {1} points", OptionsData._instance.PlayerOneName, gameResults.y);
		player2Result.text = string.Format ("{0}: {1} points", OptionsData._instance.PlayerTwoName, gameResults.x);

		this.timer = this.turnTime;
		if (!Manager.isTimeDisplayed) 
		{
			timeResult.text = string.Empty;
		} else {
			timeResult.text = string.Format ("Time: {0} s.", timer);
		}

	}

	public void SwitchPlayer()
	{
		if (player1Result.fontStyle == FontStyle.Normal) {
			player1Result.fontStyle = FontStyle.Bold;
			player2Result.fontStyle = FontStyle.Normal;
		} else {
			player1Result.fontStyle = FontStyle.Normal;
			player2Result.fontStyle = FontStyle.Bold;
		}

		this.timer = 30;
	}
	
	// Update is called once per frame
	void Update () {
		this.elapsedTime += Time.fixedTime;
		if (this.elapsedTime > 1.0f && Manager.isTimeDisplayed) {
			this.timer -= this.elapsedTime;
			this.elapsedTime = 0;
			timeResult.text = string.Format ("Time: {0} s.", (int)this.timer);

			if(this.timer < 0){
				Manager.timeout = true;
				Application.LoadLevel("EndGame_Scene");
			}
		}
	}
}
