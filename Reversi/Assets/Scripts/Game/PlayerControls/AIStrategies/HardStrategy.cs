﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.PlayerControls.AIStrategies;
using System.Linq;
using System.Collections.Generic;

public class HardStrategy : IArtificialIntelligenceStrategy
{
    public Field DetermineMove(System.Collections.Generic.Dictionary<Field, int> possibleMoves)
    {
        return possibleMoves.Where(fld => fld.Value == possibleMoves.Max(f => f.Value)).First().Key;
    }
}
