﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.PlayerControls.AIStrategies;
using System.Linq;

public class EasyStrategy : IArtificialIntelligenceStrategy
{
    public Field DetermineMove(System.Collections.Generic.Dictionary<Field, int> possibleMoves)
    {
        return possibleMoves.Where(fld => fld.Value == possibleMoves.Min(f => f.Value)).First().Key;
    }
}
