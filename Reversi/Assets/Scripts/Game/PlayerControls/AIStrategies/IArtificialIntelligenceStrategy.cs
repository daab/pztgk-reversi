﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.PlayerControls.AIStrategies
{
    public interface IArtificialIntelligenceStrategy
    {
        Field DetermineMove(Dictionary<Field, int> possibleMoves);
    }
}
