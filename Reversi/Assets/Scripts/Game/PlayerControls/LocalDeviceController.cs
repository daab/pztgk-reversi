﻿using UnityEngine;
using System.Collections;

public class LocalDeviceController : MonoBehaviour,  IInputController
{
	public void HandleInput() 
    {
        if (Input.GetKey(KeyCode.Mouse0))
        {
            Board gameBoard = GameObject.Find("GameBoard").GetComponent<Board>();
            Field field = gameBoard.HoverField;

            if(field != null && field.IsLegalMoveForActualPlayer)
            {
                GameController gameController = GameObject.Find("GameController").GetComponent<GameController>();

                gameBoard.PlacePawn(field, gameController.ActualPlayer);
				gameBoard.fireParticle(field);
                gameBoard.FlipPawns(field);
                gameController.SwitchActualPlayer();      
            }
        }
	}
}
