﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.PlayerControls.AIStrategies;

public class ArtificialIntelligenceController : MonoBehaviour, IInputController
{
    public int MoveDelay;
    
    private int MoveDelayCounter = 0;

    public IArtificialIntelligenceStrategy Strategy { get; set; }

    public void HandleInput()
    {
        if (++MoveDelayCounter >= MoveDelay)
        {
            Board gameBoard = GameObject.Find("GameBoard").GetComponent<Board>();
            GameController gameController = GameObject.Find("GameController").GetComponent<GameController>();

            List<Field> possibleMoves = gameBoard.GetPossibleMovesForActualPlayer();
            Dictionary<Field, int> movesValues = ReversiAlgorithms.CalculateMovesValues(gameBoard.Fields, gameController.ActualPlayer, possibleMoves);

            Field fieldToPlace = Strategy.DetermineMove(movesValues);

            gameBoard.PlacePawn(fieldToPlace, gameController.ActualPlayer);
			gameBoard.fireParticle(fieldToPlace);
            gameBoard.FlipPawns(fieldToPlace);
            gameController.SwitchActualPlayer();

            MoveDelayCounter = 0;
        }
    }
}
