﻿using UnityEngine;
using System.Collections;

// http://msdn.microsoft.com/en-us/library/8bc1fexb(v=vs.71).aspx
public interface IInputController
{
    void HandleInput();
}
