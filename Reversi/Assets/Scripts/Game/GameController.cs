﻿using UnityEngine;
using System.Collections;

public class GameController : MonoBehaviour
{
	public Player Player1;
	public Player Player2;

	public GameMode gameMode;

	public Player ActualPlayer { get; set; }
	
	public Board GameBoard;
	public HUD GameHUD;
	public LocalDeviceController LocalDeviceControllerPrefab;
	public ArtificialIntelligenceController ArtificialIntelligenceControllerPrefab;
	private ArtificialIntelligenceController aiController;
	
	void Awake ()
	{		
		if(PassGameMode.gameMode == GameMode.PlayerVsComputer){
			aiController = Instantiate (ArtificialIntelligenceControllerPrefab) as ArtificialIntelligenceController;
			
			switch (PassDifficulty.Difficulty) {
			case 0:			
				aiController.Strategy = new EasyStrategy ();
				break;
			case 1:			
				aiController.Strategy = new HardStrategy ();
				break;
			}
		}
	}
	
	// Use this for initialization
	void Start ()
	{
		LocalDeviceController deviceController = Instantiate (LocalDeviceControllerPrefab) as LocalDeviceController;
		
		this.Player1.InputController = deviceController;
		
		if(PassGameMode.gameMode == GameMode.PlayerVsComputer){
			this.Player2.InputController = aiController;
		}else if(PassGameMode.gameMode == GameMode.PlayerVsPlayer){
			this.Player2.InputController = deviceController;
		}else if(PassGameMode.gameMode == GameMode.Multiplayer){
			this.Player2.InputController = deviceController;
		}
		
		this.ActualPlayer = Player1;
		
		this.GameBoard = GameObject.Find ("GameBoard").GetComponent<Board> ();
	}
	
	// Update is called once per frame
	void Update ()
	{
		this.ActualPlayer.HandleControls ();
		if (!Application.isLoadingLevel) 
		{
			UpdatePoints ();
		}

	}
	
	private void SwitchPlayer ()
	{
		if (this.ActualPlayer == this.Player1) {
			this.ActualPlayer = this.Player2;
		} else {
			this.ActualPlayer = this.Player1;
		}
		
		this.GameBoard.CalculatePossibleMovesForPlayer (this.ActualPlayer);
		this.GameHUD.SwitchPlayer ();
		Manager.currentPlayer = this.ActualPlayer;
	}
	
	private void UpdatePoints()
	{
		Manager.gameResults = this.GameBoard.GetGameResults ();
		this.GameHUD.UpdateResults (Manager.gameResults);
	}
	
	public void SwitchActualPlayer ()
	{
		this.SwitchPlayer ();
		if (this.GameBoard.GetPossibleMovesForActualPlayer ().Count == 0) {
			this.SwitchPlayer ();
			if (this.GameBoard.GetPossibleMovesForActualPlayer ().Count == 0) {
				// END GAME
				Debug.Log ("END GAME");
				
				Application.LoadLevel("EndGame_Scene");
			}
		}
	}
}