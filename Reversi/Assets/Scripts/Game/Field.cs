﻿using UnityEngine;
using System.Collections;

public class Field : MonoBehaviour 
{
    public Pawn Pawn { get; set; }

    public delegate void FieldHoverDelegate(Field field);

    public FieldHoverDelegate FieldHover;

    public bool IsLegalMoveForActualPlayer { get; set; }

    // Data initializaion
    void Start () {
    }
    
    // Update is called once per frame
    void Update () 
    {
    }
    
    void OnMouseEnter()
    {
        this.FieldHover(this);
    }
    
    void OnMouseExit()
    {
        SendMessageUpwards("FieldLeave");
    }
}
