﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour 
{
    public Sprite PawnSprite;
    public IInputController InputController;

	// Use this for initialization
	void Start () 
    {
	
	}
	
	// Update is called once per frame
	void Update () 
    {
	
	}

    public void HandleControls()
    {
        this.InputController.HandleInput();
    }

    public bool IsLocalPlayer()
    {
        return InputController is LocalDeviceController;
    }
}
