﻿using UnityEngine;
using System.Collections;

public class Pawn : MonoBehaviour {
    public Player Owner { get; set; }
	Animator anim;
	// Data initialization
	void Start () {
		anim = GetComponent<Animator> ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void AssignOwner(Player player)
    {
        this.Owner = player;
        this.GetComponent<SpriteRenderer>().sprite = player.PawnSprite;
    }

	public void AssignOwnerAnim(Player player)
	{
		this.Owner = player;
		anim.SetTrigger ("turn2");
		StartCoroutine(Delay (player));

	}

	IEnumerator Delay(Player player){
		yield return new WaitForSeconds(0.5f);
		this.GetComponent<SpriteRenderer>().sprite = player.PawnSprite;
	}

}
