﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

// Algorytmy znalezione gdzies na necie
// http://wiki.gamegardens.com/Reversi_Game_Logic
public static class ReversiAlgorithms
{
    private static int[] DX = { -1, 0, 1, -1, 1, -1, 0, 1 };
    private static int[] DY = { -1, -1, -1, 0, 0, 1, 1, 1 };

    public static bool IsLegalMoveForPlayer(Field[,] fields, Player player, int i, int j)
    {
        if (fields[i, j].Pawn != null)
        {
            return false;
        }

        for (int ii = 0; ii < DX.Length; ii++)
        {
            bool sawOther = false, sawSelf = false;
            int x = i, y = j;

            for (int dd = 0; dd < 8; dd++)
            {
                x += DX[ii];
                y += DY[ii];

                // stop when we end up off the board
                if (OutBoard(x, y))
                {
                    break;
                }

                if (fields[x, y].Pawn == null)
                {
                    break;
                }
                else if (fields[x, y].Pawn.Owner == player)
                {
                    sawSelf = true;
                    break;
                }
                else
                {
                    sawOther = true;
                }
            }

            if (sawOther && sawSelf)
            {
                return true;
            }
        }

        return false;
    }

    public static void FlipPawns(Field[,] fields, Player actualPlayer, Field startField)
    {
        List<Pawn> toFlip = new List<Pawn>();

        int[] coords = CalculateFieldCoordinates(startField, fields);

        for (int ii = 0; ii < DX.Length; ii++)
        {
            int x = coords[0], y = coords[1];
            for (int dd = 0; dd < 8; dd++)
            {
                x += DX[ii];
                y += DY[ii];

                if (OutBoard(x, y))
                {
                    break;
                }

                if (fields[x, y].Pawn == null)
                {
                    break;
                }
                else if (fields[x, y].Pawn.Owner != actualPlayer)
                {
                    toFlip.Add(fields[x, y].Pawn);

                }
                else if (fields[x, y].Pawn.Owner == actualPlayer)
                {
                    foreach (Pawn pawn in toFlip)
                    {
						pawn.AssignOwnerAnim(actualPlayer);
                    }
                    break;
                }
            }

            toFlip.Clear();
        }
    }

    public static Dictionary<Field, int> CalculateMovesValues(Field[,] fields, Player actualPlayer, List<Field> possibleMoves)
    {
        Dictionary<Field, int> movesValues = new Dictionary<Field, int>();

        foreach (Field field in possibleMoves)
        {
            movesValues.Add(field, 0);
            int[] coords = CalculateFieldCoordinates(field, fields);

            // determine where this piece "captures" pieces of the opposite color
            for (int ii = 0; ii < DX.Length; ii++)
            {
                int moveScore = 0;
                // look in this direction for captured pieces
                int x = coords[0], y = coords[1];
                for (int dd = 0; dd < 8; dd++)
                {
                    x += DX[ii];
                    y += DY[ii];

                    if (OutBoard(x, y))
                    {
                        break;
                    }

                    if (fields[x, y].Pawn == null)
                    {
                        break;
                    }
                    else if (fields[x, y].Pawn.Owner != actualPlayer)
                    {
                        moveScore++;
                    }
                    else if (fields[x, y].Pawn.Owner == actualPlayer)
                    {
                        movesValues[field] += moveScore;
                    }
                }
            }
        }

        return movesValues;
    }

    private static int[] CalculateFieldCoordinates(Field field, Field[,] fields)
    {
        int[] coords = new int[2];
        coords[0] = -1;
        coords[1] = -1;

        for (int x = 0; x < 8; ++x)
        {
            for (int y = 0; y < 8; ++y)
            {
                if (fields[x, y].Equals(field))
                {
                    coords[0] = x;
                    coords[1] = y;
                }
            }
        }

        return coords;
    }

    private static bool OutBoard(int x, int y)
    {
        return x < 0 || x > 7 || y < 0 || y > 7;
    }
}
