﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Board : MonoBehaviour {
    public GameObject FieldToSpawn;
    public Pawn PawnPrefab;
    public Field[,] Fields = new Field[8, 8];
    public Field HoverField { get; set; }
    public Pawn PawnAparition;
	public GameObject Dust;

    private GameController gameController;
    private bool gameStarting;
    // Data initialization
	void Start () 
    {
        gameStarting = true;
        this.gameController = GameObject.Find("GameController").GetComponent<GameController>();
        SpawnBoardFields();
        PlaceInitialPawns();
        CalculatePossibleMovesForPlayer(this.gameController.ActualPlayer);
        gameStarting = false;
	}

	public Vector2 GetGameResults()
	{
		int player1result = 0;
		int player2result = 0;
		Pawn pawn;
		string pawnName = "";
		
		for (var i=0; i< 8; i++) {
			for (var j=0; j < 8; j++) 
			{
				pawn = Fields [i, j].Pawn;
				if (pawn) 
				{
					pawnName = pawn.Owner.PawnSprite.name.ToString();
					if(pawnName == "White Pawn"){
						player2result++;
					} else if(pawnName == "Black Pawn"){
						player1result++;
					}
				}
			}
		}
		
		return new Vector2 (player1result, player2result);
	}

    public void CalculatePossibleMovesForPlayer(Player player)
    {
        for (int i = 0; i < 8; i++)
        {
            for (int j = 0; j < 8; j++)
            {
                Fields[i, j].IsLegalMoveForActualPlayer = ReversiAlgorithms.IsLegalMoveForPlayer(Fields, this.gameController.ActualPlayer, i, j);
            }
        }
    }

	// Update is called once per frame
	void Update () {
	}

    void SpawnBoardFields()
    {
        float offsetX = (this.renderer.bounds.size.x / 2.0f) - ((FieldToSpawn.renderer.bounds.size.x / 2.0f) * this.transform.lossyScale.x) - (0.325f * this.transform.lossyScale.x);
        float offsetY = (this.renderer.bounds.size.y / 2.0f) - ((FieldToSpawn.renderer.bounds.size.y / 2.0f) * this.transform.lossyScale.y) - (0.325f * this.transform.lossyScale.y);
        float fieldsGapX = 1.05f * this.transform.lossyScale.x;
        float fieldsGapY = 1.05f * this.transform.lossyScale.y;

        for (int i = 0; i < 8; i++)
        {
            for (int j = 0; j < 8; j++)
            {
                GameObject field = Instantiate(FieldToSpawn, new Vector3(
                    this.transform.position.x + (i * fieldsGapX) - offsetX,
                    this.transform.position.y - (j * fieldsGapY) + offsetY, 
                    0),
                    this.transform.rotation) as GameObject;

                Field fieldClass = field.GetComponent<Field>();
                fieldClass.transform.parent = this.transform;

                fieldClass.transform.localScale = this.transform.lossyScale;
                fieldClass.FieldHover = this.FieldHover;

                Fields[i, j] = fieldClass;
            }
        }
    }

    void PlaceInitialPawns()
    {
        this.PlacePawn(Fields[3, 3], this.gameController.Player1);
        this.PlacePawn(Fields[4, 4], this.gameController.Player1);
        this.PlacePawn(Fields[4, 3], this.gameController.Player2);
        this.PlacePawn(Fields[3, 4], this.gameController.Player2);
    }

    void FieldHover(Field field)
    {
        if (this.gameController.ActualPlayer.IsLocalPlayer())
        {
            this.HoverField = field;
            if (field.IsLegalMoveForActualPlayer)
            {
                this.PlacePawnAparition(field, this.gameController.ActualPlayer);
            }
        }
    }

    public void FieldLeave()
    {
        this.HoverField = null;
        if (this.PawnAparition != null)
        {
            Object.Destroy(this.PawnAparition.gameObject);
        }
    }

    public void PlacePawn(Field field, Player player)
    {
        Pawn pawn = Instantiate(PawnPrefab, new Vector3(field.transform.position.x, field.transform.position.y, 0), field.transform.rotation) as Pawn;
        if (!gameStarting)
        {
			pawn.audio.volume = OptionsData._instance.EffectsVolume;//Value from options should be set here
            pawn.audio.Play();
        }
        pawn.AssignOwner(player);
        field.Pawn = pawn;
    }

    public void FlipPawns(Field field)
    {
        ReversiAlgorithms.FlipPawns(this.Fields, this.gameController.ActualPlayer, field);
    }

    private void PlacePawnAparition(Field field, Player player)
    {
        this.PawnAparition = Instantiate(PawnPrefab, new Vector3(field.transform.position.x, field.transform.position.y, 0), field.transform.rotation) as Pawn;
        this.PawnAparition.AssignOwner(player);
        Color color = this.PawnAparition.renderer.material.color;
        color.a = 0.5f;
        this.PawnAparition.renderer.material.color = color;
    }

    public List<Field> GetPossibleMovesForActualPlayer()
    {
        List<Field> possibleMoves = new List<Field>();
        for (int i = 0; i < 8; i++)
        {
            for (int j = 0; j < 8; j++)
            {
                if (Fields[i, j].IsLegalMoveForActualPlayer)
                {
                    possibleMoves.Add(Fields[i, j]);
                }
            }
        }

        return possibleMoves;
    }

	public void fireParticle(Field field){
		if(OptionsData._instance.Particle)
		Instantiate(Dust, new Vector3(field.Pawn.transform.position.x, field.Pawn.transform.position.y, field.Pawn.transform.position.z-1), Quaternion.AngleAxis(270, transform.right)*transform.rotation);
	}
}
