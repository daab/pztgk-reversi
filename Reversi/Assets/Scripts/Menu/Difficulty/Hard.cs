﻿using UnityEngine;
using System.Collections;
using AssemblyCSharp;

[RequireComponent(typeof(SpriteRenderer))]
public class Hard : MonoBehaviour
{

		private Button button;
		public SpriteRenderer sprite;
		
		void Start ()
		{
				button = new Button ("Game_Scene", sprite.color);
		}
		
		void Update ()
		{
				PassDifficulty.Difficulty = 1;
				PassGameMode.gameMode = GameMode.PlayerVsComputer;
				button.Action (Camera.main.ScreenToWorldPoint (Input.mousePosition), collider2D, this.gameObject, sprite);

		}
}
