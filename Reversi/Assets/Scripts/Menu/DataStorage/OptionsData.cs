﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class OptionsData : MonoBehaviour {
	private static OptionsData m_instance;
	
	public static OptionsData _instance {
		get {
			if (m_instance == null) {
				m_instance = new GameObject ("BetweenSceneContainer").AddComponent<OptionsData> ();          
			}
			return m_instance;
		}
	}

	public string PlayerOneName{
		get{
			return !PlayerPrefs.HasKey("Player one name") ? "Player 1" : PlayerPrefs.GetString ("Player one name");
		}
	}

	public string PlayerTwoName{
		get{
			return !PlayerPrefs.HasKey("Player two name") ? "Player 2" : PlayerPrefs.GetString ("Player two name");
		}
	}

	public float EffectsVolume{
		get{
			return !PlayerPrefs.HasKey("Effects volume") ? 10.0f : PlayerPrefs.GetFloat ("Effects volume") * 10.0f; 
		}
	}

	public bool Particle{
		get{
			return !PlayerPrefs.HasKey("Particles") ? true : (PlayerPrefs.GetInt ("Particles") >= 1 ? true : false);
		}
	}

	void Awake ()
	{
		if (m_instance == null) {
			m_instance = this;      
		} else if (m_instance != this)       
			Destroy (gameObject);    
		
		DontDestroyOnLoad (gameObject);
	}
	
	public void SaveValues (string playerOneName, string playerTwoName, float effectsVolume,int particles)
	{
		PlayerPrefs.SetString ("Player one name",playerOneName); 
		PlayerPrefs.SetString ("Player two name",playerTwoName); 
		PlayerPrefs.SetFloat ("Effects volume", effectsVolume); 
		PlayerPrefs.SetInt ("Particles", particles); 
	}
	
	public OptionsDataStorage GetValues ()
	{
		OptionsDataStorage data = new OptionsDataStorage();
		data.PlayerOneName = PlayerPrefs.GetString ("Player one name"); 
		data.PlayerTwoName = PlayerPrefs.GetString ("Player two name"); 
		data.EffectsVolume = PlayerPrefs.GetFloat ("Effects volume"); 
		data.Particles = PlayerPrefs.GetInt ("Particles");
		
		return data;
	}

	public void ResetData()
	{
		PlayerPrefs.DeleteKey ("Player one name");
		PlayerPrefs.DeleteKey ("Player two name");
		PlayerPrefs.DeleteKey ("Effects volume");
		PlayerPrefs.DeleteKey ("Particles");
	}
	
	void OnApplicationQuit()
	{
		PlayerPrefs.Save();
	}
}

public class OptionsDataStorage
{
	public string PlayerOneName;
	public string PlayerTwoName;
	public float EffectsVolume;
	public int Particles;
}