﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(GameObject))]
public class Menu : MonoBehaviour {

	public GameObject BetweenSceneContainer;

	// Use this for initialization
	void Awake () {
		Instantiate (BetweenSceneContainer);
	}
}
