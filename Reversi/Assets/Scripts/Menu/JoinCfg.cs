﻿using UnityEngine;
using System.Collections;

public class JoinCfg : MonoBehaviour
{

		public string Ip = "0.0.0.0";
		public string port = "0000";

		void OnGUI ()
		{
				Vector3 position = Camera.main.WorldToScreenPoint (new Vector3 (-5f, -2.5f, this.gameObject.transform.position.z));
		
				GUI.Label(new Rect (position.x, position.y, 200, 20),new GUIContent("Ip: "));
				Ip = GUI.TextField (new Rect (position.x, position.y + 20, 200, 20), Ip, 25);
				
				GUI.Label(new Rect (position.x, position.y + 60, 200, 20),new GUIContent("Port: "));
				port = GUI.TextField (new Rect (position.x, position.y + 80, 200, 20), port, 25);

				GUI.Button (new Rect (position.x, position.y + 120, 200, 20), new GUIContent("Proceed"));
		}
}
