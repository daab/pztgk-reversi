﻿using UnityEngine;
using System.Collections;
using AssemblyCSharp;

[RequireComponent(typeof(SpriteRenderer))]
public class NewGame : MonoBehaviour
{
		private Button button;
		public SpriteRenderer sprite;

		void Start()
		{
			button = new Button ("Difficulty_Scene", sprite.color);
		}

	    void Update()
	    {
			button.Action (Camera.main.ScreenToWorldPoint (Input.mousePosition), collider2D, this.gameObject, sprite);
		}
}
