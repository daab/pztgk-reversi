using System;
using UnityEngine;
namespace AssemblyCSharp{
		public class Button
		{
				private string nextScene;
				private Color backupColor;
				
				private static int port = 14018;
		
				public Button (Color backupColor)
				{			
						this.backupColor = backupColor;
				}
		
				public Button (string sceneToLoad, Color backupColor)
				{
						this.nextScene = sceneToLoad;
						this.backupColor = backupColor;
				}
				
				public bool InitializeServer()
				{
					Network.InitializeServer(2,port, true);
					if(Network.isServer){
						return true;
					}else{
						return false;
					}
				}
				
				public void ConnectToServer()
				{
					Network.isMessageQueueRunning = true;
					Network.Connect("127.0.0.1", port);
				}
				
				public void OnConnectedToServer()
				{
					Debug.Log("Connected to server");
					Network.isMessageQueueRunning = false;
					Application.LoadLevel(nextScene);
				}
				
				
				public void Action (Vector2 mousePosition, Collider2D collider2D, GameObject gameObject, SpriteRenderer sprite)
				{

						sprite.color = backupColor;

						Color clicked;

						if (Input.GetKeyDown (KeyCode.Escape) && Application.loadedLevelName == "MainMenu_Scene") {

								Application.Quit ();	

						} else if (Input.GetKeyDown (KeyCode.Escape) && Application.loadedLevelName != "MainMenu_Scene") {
				
								Application.LoadLevel ("MainMenu_Scene");
						}
			
						if (collider2D.OverlapPoint (mousePosition)) {
								clicked = new Color (0.5f, 0.5f, 0.5f, 1.0f);
								sprite.color = clicked;
						}

						if (Input.GetMouseButtonDown (0)) {

								if (collider2D.OverlapPoint (mousePosition) && collider2D.tag == "Button") {
					
										clicked = new Color (0.2f, 0.2f, 0.2f, 1f);
										sprite.color = clicked;
										switch (collider2D.name) {
										case "NewGame":

										case "Options":
																	
										case "Credits":
										case "X":
												Application.LoadLevel (nextScene);
												break;
										case "Multiplayer":		
											Manager.isTimeDisplayed = true;
											Application.LoadLevel (nextScene);
											break;
										case "Easy":
										case "Hard":
												Manager.isTimeDisplayed = false;
												Application.LoadLevel (nextScene);
												break;
										case "HotSeats":
												Manager.isTimeDisplayed = false;
												Application.LoadLevel(nextScene);
												break;
										case "Join":
												ConnectToServer();
												//Application.LoadLevel(nextScene);
												//int ping = Network.GetAveragePing(Network.connections[0]);
												break;
										case "Create":
											if(InitializeServer()){											
												Application.LoadLevel (nextScene);
											}
												break;
										case "Main-Menu":
												Application.LoadLevel ("MainMenu_Scene");	
												break;	
										case "Exit":
												Application.Quit ();	
												break;

										}
								}
						}
				}
		}
		
		}