﻿using UnityEngine;
using System.Collections;
using AssemblyCSharp;

[RequireComponent(typeof(SpriteRenderer))]
public class Credits : MonoBehaviour {

	private Button button;
	public SpriteRenderer sprite;
	
	void Start ()
	{
		button = new Button ("Credits_Scene", sprite.color);
	}
	
	void Update ()
	{
		button.Action (Camera.main.ScreenToWorldPoint (Input.mousePosition), collider2D, this.gameObject, sprite);
	}
}
