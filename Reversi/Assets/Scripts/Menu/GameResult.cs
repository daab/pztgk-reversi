﻿using UnityEngine;
using System.Collections;

public class GameResult : MonoBehaviour {

	public GUIText result;

	// Use this for initialization
	void Start () {
		var gameResults = Manager.gameResults;
		string results;

		if (gameResults.x == gameResults.y) {
			results = "pass";
		} else if (gameResults.x > gameResults.y) {
			results = OptionsData._instance.PlayerTwoName;
		} else {
			results = OptionsData._instance.PlayerOneName;
		}

		result.text = "Winner: " + results;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
