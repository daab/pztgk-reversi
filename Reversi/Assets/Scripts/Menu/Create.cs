﻿using UnityEngine;
using System.Collections;
using AssemblyCSharp;

[RequireComponent(typeof(SpriteRenderer))]
public class Create : MonoBehaviour {
	
	private Button button;
	public SpriteRenderer sprite;
	
	void Start ()
	{
		button = new Button ("Game_Scene", sprite.color);
	}
	
	void Update ()
	{
		PassGameMode.gameMode = GameMode.Multiplayer;
		button.Action (Camera.main.ScreenToWorldPoint (Input.mousePosition), collider2D, this.gameObject, sprite);
	}
}
