﻿using UnityEngine;
using System.Collections;

public class OptionsCfg : MonoBehaviour {

	public string playerOneName = "Player1";
	public string playerTwoName = "Player2";
	public float effectsVolumeValue = 10.0f;
	public bool particles = true;

	void Start()
	{
		playerOneName = OptionsData._instance.PlayerOneName;
		playerTwoName = OptionsData._instance.PlayerTwoName;
		effectsVolumeValue = OptionsData._instance.EffectsVolume;
		particles = OptionsData._instance.Particle;
	}

	void OnGUI() {
		Vector3 position = Camera.main.WorldToScreenPoint(new Vector3(-5f, -2.5f, this.gameObject.transform.position.z));
		playerOneName = GUI.TextField(new Rect(position.x, position.y, 200, 20), playerOneName, 25);
		playerTwoName = GUI.TextField(new Rect(position.x, position.y + 40, 200, 20), playerTwoName, 25);
		GUI.Label(new Rect (position.x, position.y + 60, 200, 20),new GUIContent("Effects: " + (effectsVolumeValue * 10).ToString("F1") + "%"));
		effectsVolumeValue = GUI.HorizontalSlider (new Rect(position.x, position.y + 80, 200, 20), effectsVolumeValue, 0.0f, 10.0f);
		particles = GUI.Toggle(new Rect(position.x, position.y + 140, 200, 20), particles, new GUIContent(" Particles"));
		OptionsData._instance.SaveValues (playerOneName, playerTwoName, effectsVolumeValue / 10.0f, particles == true? 1 : 0);
	}
}
